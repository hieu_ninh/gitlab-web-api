﻿using GitlabWebApi.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLabWebApi.Tests.ControllerTests
{
    [TestFixture]
    public class WeatherForecastControllerTests
    {
        private Mock<ILogger<WeatherForecastController>> _mockLogger;

        [SetUp] public void SetUp()
        {
            _mockLogger = new Mock<ILogger<WeatherForecastController>>();
        }
        [Test]
        public void WeatherForecastController_CreateInstance_Successfully()
        {
            //Arrange
            //Act
            var weatherforecast = new WeatherForecastController(_mockLogger.Object);
            //Assert
            Assert.IsNotNull(weatherforecast);
        }

        [Test]
        public void WeatherForecastController_Get_ReturnsForecasts()
        {
            //Arrange
            var weatherforecast = new WeatherForecastController(_mockLogger.Object);
            //Act
            var result = weatherforecast.Get();
            //Assert
            Assert.IsNotNull(result);
            Assert.That(result.Count(), Is.EqualTo(5));
            //Assert.Fail();
        }
    }
}
